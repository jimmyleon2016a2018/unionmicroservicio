import React from "react";
import styled from "styled-components";
import Socios from "./Socios";
import Libros from "./Libros";

const Container = styled.div({
  display:'flex',
  flexDirection:'row',
  minHeight:'calc(100vh)',/*ocupe toda la pantalla de alto*/
  margin:'0px',
  padding:'0px',
})
const Slider = styled.div({
  width: '35%',
  backgroundColor:'aliceblue',
  minWidth: '350px',

})

const Body = styled.div({
  width: '65%',
  minWidth: '350px',

})

function App() {


  return (
    <Container >
      <Slider>
        <Socios />
      </Slider>
      <Body>
       <Libros/>
      </Body>
    </Container>
  );
}

export default App;
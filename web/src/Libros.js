import React, { useEffect, useState } from "react";
import { socket } from "./ws";
import styled from 'styled-components';


const Container = styled.div({

})
const Libro = styled.div({

})
const Image = styled.img({
  width: '100px',
  height: '150px',
})
const App = () => {
  const [data, setData] = useState([]);
  useEffect(() => {

    console.log('esta es una prueba de libros', { data });
    socket.on('res:libros:view', ({ statusCode, data, message }) => {
      console.log('res:libros:view', { statusCode, data, message });
      if (statusCode === 200) setData(data);
      else console.log(data);

      
    });
    setTimeout(() => socket.emit('req:libros:view', ({})), 1000);

  }, []);

  return (
    <Container>
      {
        data.map((v, i) => (
          <Libro>
            <Image src={v.image}> </Image>
            <p>{v.title}</p>
            <p>{v.category}</p>
            <p>{v.seccions}</p>
          </Libro>
        ))

      }
    </Container>
  );
};

export default App;
import React, { useEffect, useState } from "react";
import { socket } from "../ws";
function Inicio() {
  
    const [data, setData] = useState([]);

    useEffect(() => {
       
        console.log('esta es una prueba', { data });
        socket.on("res:microservice:view", ({ statusCode, data, message }) => {
            console.log("res:microservice:view", { statusCode, data, message });
            if (statusCode === 200) {
                setData(data);
            };

        });
        socket.on("res:microservice:create", ({ statusCode, data, message }) => {
            console.log("res:microservice:create", { statusCode, data, message });
        });
        socket.on("res:microservice:findOne", ({ statusCode, data, message }) => {
            console.log("res:microservice:findOne", { statusCode, data, message });
        });
        socket.on("res:microservice:update", ({ statusCode, data, message }) => {
            console.log("res:microservice:update", { statusCode, data, message });
        });
        socket.on("res:microservice:delete", ({ statusCode, data, message }) => {
            console.log("res:microservice:delete", { statusCode, data, message });
        });

        setInterval(() => socket.emit('req:microservice:view', ({})), 1000);


    }, []);
    const stilosCaja = {
        backgroundColor: '#40CFFF',
        width: '70%',
        margin: 'auto'
    }
    const stilosP = {
        marginLeft: '250px'
    }
    const stilosButon = {
        backgroundColor: '#fff',
        width: '20%',
        pading: '1px',
        textAlign: 'center',
        cursor: 'pointer'
    }
    return (
        <div style={stilosCaja}>
            <p style={stilosP}>{id ? 'This online' : 'out of line'} {id}</p>
            <p style={stilosP}>QUESTION</p>

            {
                data.map((v, i) => (<ol key={i}>
                    <li>{i} </li>
                    <li>Code :{v.uuid} </li>
                    <li>Answer :{v.info} </li>
                </ol>
                ))
            }
            <br></br>
            <input type="submit" value="< Init" style={stilosButon}></input>
            <input type="submit" value="Create" style={stilosButon}></input>
            <input type="submit" value="Update" style={stilosButon}></input>
            <input type="submit" value="Delete" style={stilosButon}></input>
            <input type="submit" value="End >" style={stilosButon}></input>
        </div>
    );
}

export default Inicio;
import React, { useEffect, useState } from "react";
import { socket } from "./ws";
import styled from 'styled-components';

const Container = styled.div({
    width: '300px',
    maxWidth: '300px',
   
})
    
const Socio = styled.div({
   display: 'flex',
   flexDirection: 'column',
   padding: '10px',
})

const Body = styled.div({
   padding:'5px',
   display: 'flex',
   flexDirection: 'row',
   justifyContent: 'space-between',
   alignItems:'center'

})

const Name = styled.div({

})

const Email = styled.div({

})


const Phone = styled.div({
    
})


const Enable = styled.div`
    width: 30px;
    height: 30px;
    border-radius:50%;
    background-color: ${props =>props.enable ? 'green' :'reb'};
`

const App = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        console.log('esta es una prueba de socios : ', { data });
        socket.on("res:socios:view", ({ statusCode, data, message }) => {
            console.log("res:socios:view", { statusCode, data, message });
            if (statusCode === 200) {
                setData(data);
            }
            else console.log(message);
        });
        //setTimeout(() => socket.emit('req:socios:view', ({enable:true})), 1000);

    }, []);
    
    return (
        <Container>
            {
                data.map((v, i) => (
                    <Socio>
                        <Body>
                            <Name>{v.name}</Name>

                            <Phone>{v.phone}</Phone>
                        </Body>

                        <Body>
                            <Email>{v.email}</Email>
                            <Enable enable = {v.enable}></Enable>
                        </Body>

                    </Socio>
                ))
            }
        </Container>
    );
};

export default App;
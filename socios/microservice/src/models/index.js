const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
//este es nuestro modelo

const Model = sequelize.define("socios", {
    name: { type: DataTypes.STRING },
    age: { type: DataTypes.BIGINT },
    email: { type: DataTypes.STRING },
    phone: { type: DataTypes.STRING },
    enable: { type: DataTypes.BOOLEAN }

});
async function SyncDB() {
    try {
        await Model.sync({ logging: false });//force:true :limpia la db
        return { statusCode: 200, data: "ok" }
    } catch (error) {
        console.log(error);
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { SyncDB, Model };
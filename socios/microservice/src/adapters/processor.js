const Service = require("../services");
const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne,queueEnable,queueDisable } = require('./index');

async function Create(job, done) {

    try {
        const { name, age, email, phone,enable  } = job.data;
        let { statusCode, data, message } = await Service.Create({ name, age, email, phone,enable  });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueCreate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Delete(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Delete({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueDelete", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Update(job, done) {

    try {
        const { name, age, email, phone, id } = job.data;
        let { statusCode, data, message } = await Service.Update({ name, age, email, phone, id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueUpdate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function FindOne(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.FindOne({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueFindOne", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function View(job, done) {

    try {

        console.log('soy el view de socio',job.data);
        
        const { enable } = job.data;
        let { statusCode, data, message } = await Service.View({ enable });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueView", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Enable(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Enable({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueEnable", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Disable(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Disable({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueDisable", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};

/*esta funcion forza el arranque por esta razon hay que exportar las funciones de procesos
de esta manera un archivo externo se encarga de inicializarlo*/
async function run() {
    try {
        queueCreate.process(Create)
        queueDelete.process(Delete)
        queueUpdate.process(Update)
        queueFindOne.process(FindOne)
        queueView.process(View)
        queueEnable.process(Enable)
        queueDisable.process(Disable)

        console.log('worker Socios 100%:');

    } catch (error) {
        console.log(error);

    }
}
module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable, run }
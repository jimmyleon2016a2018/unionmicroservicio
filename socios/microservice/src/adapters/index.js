const bull = require('bull');
const { redis, name } = require('../settings');

//nos comunicamos con el mundo exterior
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull(`${name}:create`, opts)
queueDelete = bull(`${name}:delete`, opts)
queueUpdate = bull(`${name}:update`, opts)
queueFindOne = bull(`${name}:findOne`, opts)
queueView = bull(`${name}:view`, opts)
queueEnable = bull(`${name}:enable`, opts)
queueDisable = bull(`${name}:disable`, opts)

module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView, queueEnable, queueDisable }
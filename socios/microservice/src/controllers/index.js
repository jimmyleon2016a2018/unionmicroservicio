const { Model } = require("../models");

async function Create({ name, age, email, phone, enable }) {
    try {
        let instance = await Model.create({ name, age, email, phone, enable }, { logging: false })
        return { statusCode: 200, data: instance.toJSON() }
    } catch (error) {
        console.log({ step: "controllers Create", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where, logging: false })
        return { statusCode: 200, data: 'data deleted successfully' }
    } catch (error) {
        console.log({ step: "controllers Delete", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function Update({ name, age, email, phone, id }) {
    try {

        let instance = await Model.update({ name, age, email, phone }, { where: { id }, logging: false, returning: true });
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        console.log({ step: "controllers Update", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function FindOne({ where = {} }) {
    try {

        let instance = await Model.findOne({ where, logging: false });
        if (instance) return { statusCode: 200, data: instance.toJSON() }
        else return { statusCode: 400, message: "the user does not exist" }

    } catch (error) {
        console.log({ step: "controllers FindOne", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function View({where = {}}) {
    try {
        let instance = await Model.findAll({ where, logging: false })
        console.log("esta es la instancia", instance);

        return { statusCode: 200, data: instance }
    } catch (error) {
        console.log({ step: "controllers View", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};
async function Enable({ id }) {
    try {

        let instance = await Model.update({ enable: true }, { where: { id }, logging: false, returning: true });
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        console.log({ step: "controllers Enable", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function Disable({ id }) {
    try {

        let instance = await Model.update({ enable: false }, { where: { id }, logging: false, returning: true });
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        console.log({ step: "controllers Disable", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable };
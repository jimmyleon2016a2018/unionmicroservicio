const bull = require('bull');
const { name } = require("./package.json"); //capturo el nombre del microservicio lo octengo del package.json

const redis = { host: "localhost", port: 6379 }
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull(`${name.replace('api-', '')}:create`, opts)
queueDelete = bull(`${name.replace('api-', '')}:delete`, opts)
queueUpdate = bull(`${name.replace('api-', '')}:update`, opts)
queueFindOne = bull(`${name.replace('api-', '')}:findOne`, opts)
queueView = bull(`${name.replace('api-', '')}:view`, opts)
queueEnable = bull(`${name.replace('api-', '')}:enable`, opts)
queueDisable = bull(`${name.replace('api-', '')}:disable`, opts)


async function Create({ name, age, email, phone, enable }) {
    try {
        const job = await queueCreate.add({ name, age, email, phone, enable });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.log(error);
    }
};
async function Delete({ id }) {
    try {
        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.log(error);
    }
};
async function Update({ name, age, email, phone, id }) {
    try {
        const job = await queueUpdate.add({ name, age, email, phone, id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function FindOne({ id }) {
    try {
        const job = await queueFindOne.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function View({ enable }) {
    try {

        const job = await queueView.add({ enable });

        const { statusCode, data, message } = await job.finished();
        console.log(statusCode, data, message);
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function Enable({ id }) {
    try {
        const job = await queueEnable.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function Disable({ id }) {
    try {
        const job = await queueDisable.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }
## es ta es una api de socios
verifica el crub de un socio con dos metodos de enable y disable

socket.on('req:socios:view', async ({ enable }) => {
    try {
        console.log('req:socios:view');
        const { statusCode, data, message } = await apiSocios.View({ enable });
        return io.to(socket.id).emit('res:socios:view', ({ statusCode, data, message }));

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:create', async ({ name, age, email, phone }) => {
    try {
        console.log('req:socios:create', { name, age, email, phone });
        const { statusCode, data, message } = await apiSocios.Create({ name, age, email, phone });
        return io.to(socket.id).emit('res:socios:create', ({ statusCode, data, message }));

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:findOne', async ({ id }) => {
    try {
        console.log('req:socios:findOne', { id });
        const { statusCode, data, message } = await apiSocios.FindOne({ id });
       return io.to(socket.id).emit('res:socios:findOne', ({ statusCode, data, message }))

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:update', async ({ name, age, email, phone, id }) => {
    try {
        console.log('req:socios:update', { name, age, email, phone, id });
        const { statusCode, data, message } = await apiSocios.Update({ name, age, email, phone, id });
        return io.to(socket.id).emit('res:socios:update', ({ statusCode, data, message }))

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:delete', async ({ id }) => {
    try {
        console.log('req:socios:delete', { id });
        const { statusCode, data, message } = await apiSocios.Delete({ id });
        return io.to(socket.id).emit('res:socios:delete', ({ statusCode, data, message }))

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:enable', async ({ name, age, color, id }) => {
    try {
        console.log('req:socios:enable', { id });
        const { statusCode, data, message } = await apiSocios.Enable({ id });
        return io.to(socket.id).emit('res:socios:enable', ({ statusCode, data, message }))

    } catch (error) {
        console.log(error);

    }
})

socket.on('req:socios:update', async ({ name, age, color, id }) => {
    try {
        console.log('req:socios:update', { id });
        const { statusCode, data, message } = await apiSocios.Disable({ id });
        return io.to(socket.id).emit('res:socios:update', ({ statusCode, data, message }))

    } catch (error) {
        console.log(error);

    }
})


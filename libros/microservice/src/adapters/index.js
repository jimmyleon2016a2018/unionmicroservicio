const bull = require('bull');
const { redis,name } = require('../settings');

const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull(`${name}:create`, opts)
queueDelete = bull(`${name}:delete`, opts)
queueUpdate = bull(`${name}:update`, opts)
queueFindOne = bull(`${name}:findOne`, opts)
queueView = bull(`libros:view`, opts)

module.exports = { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView }
const Service = require("../services");
const { queueView, queueCreate, queueDelete, queueUpdate, queueFindOne } = require('./index');

async function Create(job, done) {

    try {
        const { title, category,  seccions,image } = job.data;
        let { statusCode, data, message } = await Service.Create({ title, category,  seccions,image });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueCreate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Delete(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Delete({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueDelete", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Update(job, done) {

    try {
        const { title, category,  seccions, id } = job.data;
        let { statusCode, data, message } = await Service.Update({title, category,  seccions, id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueUpdate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function FindOne(job, done) {

    try {
        const { title } = job.data;
        let { statusCode, data, message } = await Service.FindOne({ title });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueFindOne", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function View(job, done) {
    
    try {
        console.log('llamando al view de libros');
        
        let { statusCode, data, message } = await Service.View({});
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueView", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};

/*esta funcion forza el arranque por esta razon hay que exportar las funciones de procesos
de esta manera un archivo externo se encarga de inicializarlo*/
async function run() {
    try {
        queueCreate.process(Create)
        queueDelete.process(Delete)
        queueUpdate.process(Update)
        queueFindOne.process(FindOne)
        queueView.process(View)

        console.log('worker Libros 100% :');

    } catch (error) {
        console.log(error);

    }
}
module.exports = { Create, Delete, Update, FindOne, View, run }
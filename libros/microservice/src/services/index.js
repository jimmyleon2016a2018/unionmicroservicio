const Controllers = require("../controllers");
const { InternalError } = require("../settings");

async function Create({ title, category, seccions, image}) {
    try {
        let { statusCode, data, message } = await Controllers.Create({title, category, seccions, image });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service Create", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function Delete({ id }) {
    try {
        //buscar el usuario para ver si existe 
        const findOne = await Controllers.FindOne({ where: { id } });
        if (findOne.statusCode !== 200) {
            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "the user does not exist" }
                default: return { statusCode: 500, message: InternalError }
            }
            
        }

        let del = await Controllers.Delete({ where: { id } });
        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };
        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: " service Delete", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function Update({ title, category,  seccions, id }) {
    try {
        let { statusCode, data, message } = await Controllers.Update({ title, category,  seccions, id });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service Update", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function FindOne({ title }) {
    try {
        let { statusCode, data, message } = await Controllers.FindOne({ where: { title } });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service FindOne", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function View({}) {
    try {
        let { statusCode, data, message } = await Controllers.View({});
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service View", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};

module.exports = { Create, Delete, Update, FindOne, View };
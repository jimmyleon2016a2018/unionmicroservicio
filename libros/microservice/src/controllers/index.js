const { Model } = require("../models");

async function Create({ title, category, seccions, image }) {
    try {
        let instance = await Model.create({ title, category, seccions, image}, { logging: false })
        return { statusCode: 200, data: instance.toJSON() }
    } catch (error) {
        console.log({ step: "controllers Create", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where, logging: false })
        return { statusCode: 200, data: 'data deleted successfully' }
    } catch (error) {
        console.log({ step: "controllers Delete", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }
};

async function Update({title, category,  seccions, id }) {
    try {

        let instance = await Model.update({ title, category,  seccions }, { where: { id }, logging: false,returning:true });
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        console.log({ step: "controllers Update", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

async function FindOne({ where = {} }) {
    try {

        let instance = await Model.findOne({ where, logging: false });
        if (instance) return { statusCode: 200, data: instance.toJSON() }
        else return { statusCode: 400, message: "the libro does not exist" }

    } catch (error) {
        console.log({ step: "controllers FindOne", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

async function View() {
    try {
        let instance = await Model.findAll({logging: false })
        console.log('reguistross',instance);
        
        return { statusCode: 200, data: instance }
    } catch (error) {
        console.log({ step: "controllers View", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

module.exports = { Create, Delete, Update, FindOne, View };
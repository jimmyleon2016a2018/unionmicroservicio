## es ta es una api de libros

esta api interactura con los libros de la biblioteca
estas funciones son con las que trabaja mi api de libros

```js

    const apiLibros = require('api-libros');

    socket.on('req:libros:view', async ({ }) => {
        try {
            console.log('req:libros:view');
            const { statusCode, data, message } = await apiLibros.View({});
            return io.to(socket.id).emit('res:libros:view', ({ statusCode, data, message }));

        } catch (error) {
            console.log(error);

            }

        })

    socket.on('req:libros:create', async ({ title, category, seccions }) => {
        try {
            console.log('req:libros:create', { title, category, seccions });
            const { statusCode, data, message } = await apiLibros.Create({ title, category, seccions });
            return io.to(socket.id).emit('res:libros:create', ({ statusCode, data, message }));

        }catch (error) {
            console.log(error);

            }
        })

    socket.on('req:libros:findOne', async ({ title }) => {
        try {
        console.log('req:libros:findOne', { title });
            const { statusCode, data, message } = await apiLibros.FindOne({ title });
            return io.to(socket.id).emit('res:libros:findOne', ({ statusCode, data, message }))

        } catch (error) {
            console.log(error);

        }

    })

    socket.on('req:libros:update', async ({ title, category, seccions, id }) => {
        try {
            console.log('req:libros:update', { title, category, seccions, id });
            const { statusCode, data, message } = await apiLibros.Update({ title, category, seccions, id });
            return io.to(socket.id).emit('res:libros:update', ({ statusCode, data, message }))

        } catch (error) {
            console.log(error);

            }
        })
        
    socket.on('req:libros:delete', async ({ id }) => {
        try {
            console.log('req:libros:delete', { id });
            const { statusCode, data, message } = await apiLibros.Delete({ id });
            return io.to(socket.id).emit('res:libros:delete', ({ statusCode, data, message }))

        } catch (error) {
            console.log(error);

        }
    })


```
const bull = require('bull');
const { name } = require("./package.json"); 

const redis = { host: "localhost", port: 6379 }
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull(`${name.replace('api-','')}:create`, opts)
queueDelete = bull(`${name.replace('api-','')}:delete`, opts)
queueUpdate = bull(`${name.replace('api-','')}:update`, opts)
queueFindOne = bull(`${name.replace('api-','')}:findOne`, opts)
queueView = bull(`libros:view`, opts)

async function Create({ title, category, seccions, image }) {
    try {
        const job = await queueCreate.add({ title, category, seccions, image });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function Delete({ id }) {
    try {
        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.log(error);
    }
};
async function Update({  title, category,  seccions, id }) {
    try {
        const job = await queueUpdate.add({  title, category,  seccions, id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function FindOne({ title }) {
    try {
        const job = await queueFindOne.add({ title });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};
async function View({}) {
    try {
        console.log('view api');
        
        const job = await queueView.add({});
        console.log('job',job);
        

        const { statusCode, data, message } = await job.finished();
        console.log(statusCode, data, message);
        return { statusCode, data, message };

    } catch (error) {
        console.log(error);
    }
};

module.exports = { Create, Delete, Update, FindOne, View }
const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');
//este es nuestro modelo

const Model = sequelize.define("pagos", {
    socio: { type: DataTypes.BIGINT },
    amount: { type: DataTypes.BIGINT }
    
});
async function SyncDB() {
    try {
        await Model.sync({ logging: false ,force:true });   
        return { statusCode: 200, data: "ok" }
    } catch (error) {
        console.log(error);
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { SyncDB, Model };
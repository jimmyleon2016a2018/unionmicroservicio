const Service = require("../services");
const { queueView, queueCreate, queueDelete, queueFindOne } = require('./index');

async function Create(job, done) {

    try {
        const { socio, amount } = job.data;
        let { statusCode, data, message } = await Service.Create({ socio, amount });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueCreate", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function Delete(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.Delete({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueDelete", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function FindOne(job, done) {

    try {
        const { id } = job.data;
        let { statusCode, data, message } = await Service.FindOne({ id });
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueFindOne", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};
async function View(job, done) {

    try {
        const { } = job.data;

        let { statusCode, data, message } = await Service.View({});
        done(null, { statusCode, data, message })

    } catch (error) {
        console.log({ step: "adapter process queueView", error: error.toString() });
        done(null, { statusCode: 500, message: InternalError })
    }

};

async function run() {
    try {
        queueCreate.process(Create)
        queueDelete.process(Delete)
        queueFindOne.process(FindOne)
        queueView.process(View)

        console.log('worker Pagos 100% :');

    } catch (error) {
        console.log(error);

    }
}
module.exports = { Create, Delete, FindOne, View, run }
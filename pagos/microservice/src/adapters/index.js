const bull = require('bull');
const { redis } = require('../settings');

//nos comunicamos con el mundo exterior
const opts = { redis: { host: redis.host, port: redis.port } };

queueCreate = bull("pagos:create", opts)
queueDelete = bull("pagos:delete", opts)
queueFindOne = bull("pagos:findOne", opts)
queueView = bull("pagos:view", opts)

module.exports = { queueCreate, queueDelete, queueFindOne, queueView }
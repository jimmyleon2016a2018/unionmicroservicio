const { Model } = require("../models");

async function Create({ socio, amount}) {
    try {
        let instance = await Model.create({ socio, amount }, { logging: false })
        return { statusCode: 200, data: instance.toJSON() }
    } catch (error) {
        console.log({ step: "controllers Create", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function Delete({ where = {} }) {
    try {
        await Model.destroy({ where, logging: false })
        return { statusCode: 200, data: 'data deleted successfully' }
    } catch (error) {
        console.log({ step: "controllers Delete", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function FindOne({ where = {} }) {
    try {

        let instance = await Model.findOne({ where, logging: false });
        if (instance) return { statusCode: 200, data: instance.toJSON() }
        else return { statusCode: 400, message: "the amount does not exist" }

    } catch (error) {
        console.log({ step: "controllers FindOne", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};
async function View(where = {}) {
    try {
        let instance = await Model.findAll({ where, logging: false })
        return { statusCode: 200, data: instance }
    } catch (error) {
        console.log({ step: "controllers View", error: error.toString() });
        return { statusCode: 400, message: error.toString() };
    }
};

module.exports = { Create, Delete, FindOne, View};
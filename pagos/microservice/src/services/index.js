const Controllers = require("../controllers");
const { InternalError } = require("../settings");

async function Create({ socio, amount }) {
    try {
        let { statusCode, data, message } = await Controllers.Create({ socio, amount });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service Create", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function Delete({ id }) {
    try {
        const findOne = await Controllers.FindOne({ where: { id } });
        if (findOne.statusCode !== 200) {
           
            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: "the amount does not exist" }
                default: return { statusCode: 500, message: InternalError }
            }
            
        }

        let del = await Controllers.Delete({ where: { id } });
        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data };
        return { statusCode: 400, message: InternalError };
    } catch (error) {
        console.log({ step: " service Delete", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function FindOne({ id }) {
    try {
        let { statusCode, data, message } = await Controllers.FindOne({ where: { id } });
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service FindOne", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};
async function View({ }) {
    try {
        let { statusCode, data, message } = await Controllers.View({});
        return { statusCode, data, message };
    } catch (error) {
        console.log({ step: " service View", error: error.toString() });
        return { statusCode: 500, message: error.toString() };
    }

};

module.exports = { Create, Delete, FindOne, View };